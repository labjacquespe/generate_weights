import unittest
import generate_weights
from io import StringIO
from argparse import ArgumentError
from contextlib import redirect_stderr
import re


class Test_uniform_function(unittest.TestCase):
    def setUp(self):
        pass

    def test_uniform_none(self):
        ret = generate_weights.uniform(None)
        self.assertIsInstance(ret, list)
        self.assertEqual(ret, [])

    def test_uniform_zero(self):
        ret = generate_weights.uniform(0)
        self.assertIsInstance(ret, list)
        self.assertEqual(ret, [])

    def test_uniform_n(self):
        ret = generate_weights.uniform(3)
        self.assertIsInstance(ret, list)
        self.assertEqual(ret, [1, 1, 1])


class Test_gaussian_function(unittest.TestCase):
    def setUp(self):
        pass

    def format(self, ret):
        return ['{:0.6f}'.format(i) for i in ret]

    def test_gaussian_nones(self):
        ret = generate_weights.gaussian(None, None, None, None)
        self.assertIsInstance(ret, list)
        self.assertEqual(ret, [])

    def test_gaussian_zeros(self):
        ret = generate_weights.gaussian(0, 0, 0, 0)
        self.assertIsInstance(ret, list)
        self.assertEqual(ret, [])

    def test_gaussian_ones(self):
        ret = generate_weights.gaussian(1, 1, 1, 1)
        self.assertIsInstance(ret, list)
        self.assertEqual(self.format(ret), ['0.606531'])

    def test_gaussian_many(self):
        ret = generate_weights.gaussian(nvalues=5, phi=0.1, mu=0, sigma=5)
        self.assertIsInstance(ret, list)
        self.assertEqual(self.format(ret), ['0.100000', '0.098020', '0.092312', '0.083527', '0.072615'])


class Test_create_parser_function(unittest.TestCase):
    def setUp(self):
        self.parser = generate_weights.create_parser()

    def test_noargs(self):
        with redirect_stderr(StringIO()):
            with self.assertRaises(SystemExit):
                with self.assertRaises(ArgumentError):
                    self.parser.parse_args([])

    def test_function_required(self):
        ret = self.parser.parse_args(['--function', 'gaussian', '--ncurves', '1', '--nvalues', '1', '--phis', '1', '--mus', '1', '--sigmas', '1'])
        self.assertIsInstance(ret.function, str)
        self.assertIsInstance(ret.ncurves, int)

        self.assertIsInstance(ret.nvalues, list)
        self.assertTrue(all(isinstance(e, int) for e in ret.nvalues))

        self.assertIsInstance(ret.phis, list)
        self.assertTrue(all(isinstance(e, float) for e in ret.phis))

        self.assertIsInstance(ret.mus, list)
        self.assertTrue(all(isinstance(e, float) for e in ret.mus))

        self.assertIsInstance(ret.sigmas, list)
        self.assertTrue(all(isinstance(e, float) for e in ret.sigmas))


class Test_parse_args_function(unittest.TestCase):
    def setUp(self):
        self.parser = generate_weights.create_parser()
        self.usage = "usage: test.py [-h] -f {gaussian,uniform} -c NCURVES -v NVALUES [NVALUES ...] -p PHIS [PHIS ...] -m MUS [MUS ...] -s SIGMAS [SIGMAS ...] "
        self.msg = "test.py: error: argument {arg}: invalid number of values: {m} (must have {n} values) "
        self.ncurves = 2

    def parse(self, args):
        sio = StringIO()
        with redirect_stderr(sio):
            with self.assertRaises(SystemExit):
                with self.assertRaises(ArgumentError):
                    generate_weights.parse_args(self.parser, args)

        # Remove multiple spaces and newlines for easier equality
        return re.sub(' +', ' ', sio.getvalue().replace('\n', ' '))

    def test_valid(self):
        values = ['1'] * self.ncurves
        args = ['--function', 'gaussian', '--ncurves', str(self.ncurves), '--nvalues', *values, '--phis', *values, '--mus', *values, '--sigmas', *values]
        generate_weights.parse_args(self.parser, args)

    def test_invalid_nvalues(self):
        args = ['--function', 'gaussian', '--ncurves', str(self.ncurves), '--nvalues', '1', '--phis', '1', '--mus', '1', '--sigmas', '1']
        self.assertEqual(self.parse(args), self.usage + self.msg.format(arg="/".join(generate_weights.NVALUES_ARG), m=1, n=self.ncurves))

    def test_invalid_phis(self):
        args = ['--function', 'gaussian', '--ncurves', str(self.ncurves), '--nvalues', '1', '1', '--phis', '1', '--mus', '1', '--sigmas', '1']
        self.assertEqual(self.parse(args), self.usage + self.msg.format(arg="/".join(generate_weights.PHIS_ARG), m=1, n=self.ncurves))

    def test_invalid_mus(self):
        args = ['--function', 'gaussian', '--ncurves', str(self.ncurves), '--nvalues', '1', '1', '--phis', '1', '1', '--mus', '1', '--sigmas', '1']
        self.assertEqual(self.parse(args), self.usage + self.msg.format(arg="/".join(generate_weights.MUS_ARG), m=1, n=self.ncurves))

    def test_invalid_sigmas(self):
        args = ['--function', 'gaussian', '--ncurves', str(self.ncurves), '--nvalues', '1', '1', '--phis', '1', '1', '--mus', '1', '1', '--sigmas', '1']
        self.assertEqual(self.parse(args), self.usage + self.msg.format(arg="/".join(generate_weights.SIGMAS_ARG), m=1, n=self.ncurves))


if __name__ == '__main__':
    unittest.main()
