# Weights Generator : VAP Utils

Copyright (C) 2015-2018 Charles Coulombe
Université de Sherbrooke. All rights reserved.

Run this tool to generate a weights file to use with [vap_sim](https://bitbucket.org/labjacquespe/vap_sim).

## Installation
Latest from the git repository:
```
$ python3 -m venv venv
$ source venv/bin/activate
(venv) $ pip install git+https://bitbucket.org/labjacquespe/generate_weights
```

A specific version:
```
$ git clone https://bitbucket.org/labjacquespe/generate_weights -b v1.0.0
$ python3 -m venv generate_weights/venv
$ source generate_weights/venv/bin/activate
(venv) $ pip install .
```

## Usage
```
-h [ --help ]         show help message
-f [ --function ] arg function to use: gaussian or uniform
-c [ --ncurves ] arg  number of bell curves
-v [ --nvalues ] arg  array of number of values to generate
-p [ --phis ] arg     array of phis, height of the bell curves
-m [ --mus ] arg      array of mus, center of the bell curves
-s [ --sigmas ] arg   array of sigmas, deviation of the bell curves
```

## Dependencies
```
python 3
```

## Run
From the virtual environment:
```
(venv) $ generate_weights --function gaussian --ncurves 2 --nvalues 270 270 --phis 10 10 --mus 121 149 --sigmas 67.5 67.5 > weights_file.txt
```
Or you can directly the script with the interpreter: `python3 generate_weights.py ...`

## Bug Reports and Feedback
You can report a bug [here](https://bitbucket.org/labjacquespe/generate_weights/issues).
You can also directly contact us via email by writing to vap_support at usherbrooke dot ca.

Any and all feedback is welcome.

## Licensing
VAP is released under the [GPL license](http://www.gnu.org/licenses/gpl-3.0.txt). For further details see the LICENSE file in this directory.
