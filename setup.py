#!/usr/bin/env python

import generate_weights
from setuptools import setup


""" Get README.md content. """
with open("README.md", 'r') as f:
    long_description = f.read()

""" Get LICENSE content. """
with open("LICENSE", 'r') as f:
    license = f.read()

setup(
    name="generate_weights",
    version=generate_weights.__version__,
    py_modules=["generate_weights"],
    description="Generate weights for vap_sim utility.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    license=license,
    url="https://bitbucket.org/labjacquespe/generate_weights",
    author="Charles Coulombe",
    python_requires='>=3.6',
    entry_points={
        'console_scripts': [
            'generate_weights = generate_weights:main',
        ],
    }
)
