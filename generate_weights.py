#!/bin/env python3

import argparse
import sys
from math import exp

__version__ = "2.0.0"

FUNCTION_ARG = ("-f", "--function")
NCURVES_ARG = ("-c", "--ncurves")
NVALUES_ARG = ("-v", "--nvalues")
PHIS_ARG = ("-p", "--phis")
MUS_ARG = ("-m", "--mus")
SIGMAS_ARG = ("-s", "--sigmas")

FUNCTION_CHOICES = ["gaussian", "uniform"]


def uniform(nvalues):
    """ Create a list of uniform values of weight "1". """
    return [1 for _ in range(nvalues or 0)]


def gaussian(nvalues, phi, mu, sigma):
    """ Generate a gaussian function based on parameters: phi, mu and sigma. """
    def _gaussian(x, phi, mu, sigma):
        return phi * exp(-0.5 * ((x - mu) / sigma) ** 2)

    return [_gaussian(i, phi, mu, sigma) for i in range(nvalues or 0)]


def create_parser():
    """ Create the argument parser. """
    parser = argparse.ArgumentParser(prog="generate_weights",
                                     description="Generate weights for vap_sim utility.",
                                     epilog="Report bugs to: <https://bitbucket.org/labjacquespe/generate_weights/issues>")

    parser.add_argument(FUNCTION_ARG[0], FUNCTION_ARG[1], required=True, choices=FUNCTION_CHOICES, help="function to use: gaussian or uniform.")
    parser.add_argument(NCURVES_ARG[0], NCURVES_ARG[1], required=True, type=int, help="number of bell curves")
    parser.add_argument(NVALUES_ARG[0], NVALUES_ARG[1], required=True, type=int, nargs='+', help="array of number of values to generate")
    parser.add_argument(PHIS_ARG[0], PHIS_ARG[1], required=True, type=float, nargs='+', help="array of phis, height of the bell curves")
    parser.add_argument(MUS_ARG[0], MUS_ARG[1], required=True, type=float, nargs='+', help="array of mus, center of the bell curves")
    parser.add_argument(SIGMAS_ARG[0], SIGMAS_ARG[1], required=True, type=float, nargs='+', help="array of sigmas, deviation of the bell curves")

    parser.add_argument('--version', action='version', version=__version__)

    return parser


def parse_args(parser, args=sys.argv[1:]):
    """ Parse command line arguments. """
    args = parser.parse_args(args=args)

    msg = "argument {arg}: invalid number of values: {m} (must have {n} values)"

    for short_name, long_name in [NVALUES_ARG, PHIS_ARG, MUS_ARG, SIGMAS_ARG]:
        arg = getattr(args, long_name[2:])
        if args.ncurves != len(arg):
            parser.error(msg.format(arg="{s}/{l}".format(s=short_name, l=long_name), m=len(arg), n=args.ncurves))

    return args


def main():
    args = parse_args(create_parser())

    for n in range(args.ncurves):
        weights = uniform(args.nvalues[n]) if args.function == "uniform" else gaussian(args.nvalues[n], args.phis[n], args.mus[n], args.sigmas[n])

        print("\n".join('{:0.6f}'.format(i) for i in weights))


if __name__ == "__main__":
    main()
